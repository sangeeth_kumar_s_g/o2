// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var App = angular.module('starter', ['ionic','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

App.controller("AppController",function($scope, $cordovaDialogs, $cordovaCamera, $cordovaBarcodeScanner) {

$scope.Dialog = function(){
 
  $cordovaDialogs.confirm('Clue solved successfully. Earn bonus points', 'Correct Answer', ['Earn Bonus Points'])
    .then(function(buttonIndex) {
      // no button = 0, 'OK' = 1, 'Cancel' = 2
      var btnIndex = buttonIndex;
	  if (btnIndex==1)
	  {
		  
				 // alert('Take Selfie');
				  var options = {
                     quality: 50,
                     destinationType: Camera.DestinationType.DATA_URL,
                     sourceType: Camera.PictureSourceType.CAMERA,
                     allowEdit: true,
                     encodingType: Camera.EncodingType.JPEG,
                     targetWidth: 100,
                     targetHeight: 100,
                     popoverOptions: CameraPopoverOptions,
                     saveToPhotoAlbum: true
                   };
				   
				     $cordovaCamera.getPicture(options).then(function(imageData) {
						 alert('selfie taken :)');
                     //$scope.imgSrc = "data:image/jpeg;base64," + imageData;
                      }, function(err) {
						  alert('error ' +err);
                        console.log(err);
                       });
			  
			  
			  /*
			  else
			  
                   {
					  // alert('Scanner');
					   
                      $cordovaBarcodeScanner.scan()
					  .then(
					        function(imageData) 
							{
                              alert(imageData.text);
                              console.log("Barcode Format -> " + imageData.format);
                              console.log("Cancelled -> " + imageData.cancelled);
                             },
					          function(error) {
						      alert('Error ' + error);
                              console.log("An error happened -> " + error); }
                           );
				       
			        }
				  */
	
	  }
	});


  // beep once
  $cordovaDialogs.beep(1);


};

$scope.scanBarcode = function() {
        $cordovaBarcodeScanner.scan().then(function(imageData) {
            alert(imageData.text);
            console.log("Barcode Format -> " + imageData.format);
            console.log("Cancelled -> " + imageData.cancelled);
        }, function(error) {
            console.log("An error happened -> " + error);
        });
    };
});